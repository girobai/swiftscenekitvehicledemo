//
//  GameSCNView.swift
//  SwiftSceneKitVehicleDemo
//
//  Created by Matteo Cocon on 18/01/15.
//  Copyright (c) 2015 Matteo Cocon. All rights reserved.
//

import SceneKit

class GameSCNView: SCNView {
  
  var touchCount: Int = 0
  var inCarView: Bool = false
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    let touch: AnyObject? = touches.anyObject()
    
    // update the total number of touches on screen
    if let allTouches = event.allTouches() {
      touchCount = allTouches.count
    } else {
      fatalError("unable to count number of touch on screen")
    }
  }
  
  
  override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
    touchCount = 0 // reset touch counter
  }
}
