# Swift SceneKit Vehicle Demo #

This sample code shows how to simulate a vehicle using the SCNPhysicsVehicle behaviour. The vehicle can be controller with either the accelerometer or a game controller. It also illustrate basic physics interaction and game overlays done with SpriteKit. The code is written in swift following the [SceneKit Vehicle Demo](https://developer.apple.com/library/ios/samplecode/SceneKitVehicle/Introduction/Intro.html) from [Apple iOS Developer Library](https://developer.apple.com/library)
